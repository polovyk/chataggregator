﻿using ChatAggregator.BL.Data.Entities;
using ChatAggregator.BL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChantAggregator.BL.Tests
{
    public static class ChatEventServiceTestsData
    {
        public static List<User> TestUsersList { get; } = new List<User>
        {
            new User { Id = 1, Name = "Bob" },
            new User { Id = 2, Name = "Kate" },
            new User { Id = 3, Name = "Mike" },
            new User { Id = 4, Name = "Den" }
        };

        public static List<Comment> TestCommentsList { get; } = new List<Comment>
        {
            new Comment { ChatEventId = 3, Text = "Hey, Kate - high five?" },
            new Comment { ChatEventId = 6, Text = "Oh, typical" },
            new Comment { ChatEventId = 9, Text = "Hello!" },
            new Comment { ChatEventId = 13, Text = "Bye!" },
            new Comment { ChatEventId = 18, Text = "Hello!" },
            new Comment { ChatEventId = 19, Text = "Hello!" }
        };

        public static List<HighFive> TestHighFivesList { get; } = new List<HighFive>
        {
            new HighFive { ChatEventId = 4, HighFivedUserId = 1, HighFivedUser = TestUsersList.First(e=> e.Id == 1) },
            new HighFive { ChatEventId = 11, HighFivedUserId = 4, HighFivedUser = TestUsersList.First(e=> e.Id == 4) },
            new HighFive { ChatEventId = 12, HighFivedUserId = 3, HighFivedUser = TestUsersList.First(e=> e.Id == 3) },
            new HighFive { ChatEventId = 20, HighFivedUserId = 3, HighFivedUser = TestUsersList.First(e=> e.Id == 3) },
            new HighFive { ChatEventId = 22, HighFivedUserId = 2, HighFivedUser = TestUsersList.First(e=> e.Id == 2) }
        };

        public static List<ChatEvent> TestChatEventsList { get; } = new List<ChatEvent>
        {
            new ChatEvent() { Id = 1, UserId = 1, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime (2020, 10, 21, 17, 00, 00), User = TestUsersList.First(e=> e.Id == 1) },
            new ChatEvent() { Id = 2, UserId = 2, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime(2020, 10, 21, 17, 5, 00), User = TestUsersList.First(e=> e.Id == 2) },
            new ChatEvent() { Id = 3, UserId = 1, ChatEventType = ChatEventType.Comment, OccuredTime = new DateTime(2020, 10, 21, 17, 15, 00), User = TestUsersList.First(e=> e.Id == 1)
                , Comment = TestCommentsList.First(e=> e.ChatEventId == 3) },
            new ChatEvent() { Id = 4, UserId = 2, ChatEventType = ChatEventType.HighFiveAnotherPerson, OccuredTime = new DateTime(2020, 10, 21, 17, 17, 00), User = TestUsersList.First(e=> e.Id == 2)
                , HighFive =  TestHighFivesList.First(e=> e.ChatEventId == 4) },
            new ChatEvent() { Id = 5, UserId = 1, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 21, 17, 18, 00), User = TestUsersList.First(e=> e.Id == 1) },
            new ChatEvent() { Id = 6, UserId = 2, ChatEventType = ChatEventType.Comment, OccuredTime = new DateTime(2020, 10, 21, 17, 20, 00), User = TestUsersList.First(e=> e.Id == 2)
                , Comment = TestCommentsList.First(e=> e.ChatEventId == 6) },
            new ChatEvent() { Id = 7, UserId = 2, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 21, 17, 21, 00), User = TestUsersList.First(e=> e.Id == 2) },

            new ChatEvent() { Id = 8, UserId = 3, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime(2020, 10, 21, 17, 59, 20), User = TestUsersList.First(e=> e.Id == 3) },
            new ChatEvent() { Id = 9, UserId = 3, ChatEventType = ChatEventType.Comment, OccuredTime = new DateTime(2020, 10, 21, 18, 21, 00), User = TestUsersList.First(e=> e.Id == 3)
                , Comment = TestCommentsList.First(e=>e.ChatEventId == 9) },
            new ChatEvent() { Id = 10, UserId = 4, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime(2020, 10, 21, 18, 21, 10), User = TestUsersList.First(e=> e.Id == 4) },
            new ChatEvent() { Id = 11, UserId = 3, ChatEventType = ChatEventType.HighFiveAnotherPerson, OccuredTime = new DateTime(2020, 10, 21, 18, 21, 30), User = TestUsersList.First(e=> e.Id == 3)
                , HighFive = TestHighFivesList.First(e=> e.ChatEventId == 11) },
            new ChatEvent() { Id = 12, UserId = 4, ChatEventType = ChatEventType.HighFiveAnotherPerson, OccuredTime = new DateTime(2020, 10, 21, 18, 22, 00), User = TestUsersList.First(e=> e.Id == 4)
                , HighFive = TestHighFivesList.First(e=> e.ChatEventId == 12) },
            new ChatEvent() { Id = 13, UserId = 3, ChatEventType = ChatEventType.Comment, OccuredTime = new DateTime(2020, 10, 21, 18, 23, 00), User = TestUsersList.First(e=> e.Id == 3)
                , Comment = TestCommentsList.First(e=> e.ChatEventId == 13) },
            new ChatEvent() { Id = 14, UserId = 3, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 21, 18, 23, 10), User = TestUsersList.First(e=> e.Id == 4) },
            new ChatEvent() { Id = 15, UserId = 4, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 21, 18, 23, 57), User = TestUsersList.First(e=> e.Id == 4) },

            new ChatEvent() { Id = 16, UserId = 1, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime(2020, 10, 22, 17, 21, 00), User = TestUsersList.First(e=> e.Id == 1) },
            new ChatEvent() { Id = 17, UserId = 3, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime(2020, 10, 22, 17, 22, 00), User = TestUsersList.First(e=> e.Id == 3) },
            new ChatEvent() { Id = 18, UserId = 1, ChatEventType = ChatEventType.Comment, OccuredTime = new DateTime(2020, 10, 22, 17, 23, 00), User = TestUsersList.First(e=> e.Id == 1)
                , Comment = TestCommentsList.First(e=> e.ChatEventId == 18)},
            new ChatEvent() { Id = 19, UserId = 3, ChatEventType = ChatEventType.Comment, OccuredTime = new DateTime(2020, 10, 22, 17, 24, 00), User = TestUsersList.First(e=> e.Id == 3)
                , Comment = TestCommentsList.First(e=> e.ChatEventId == 19)},
            new ChatEvent() { Id = 20, UserId = 1, ChatEventType = ChatEventType.HighFiveAnotherPerson, OccuredTime = new DateTime(2020, 10, 22, 17, 25, 00), User = TestUsersList.First(e=> e.Id == 1)
                , HighFive = TestHighFivesList.First(e=> e.ChatEventId == 20) },
            new ChatEvent() { Id = 21, UserId = 2, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime(2020, 10, 22, 17, 26, 00), User = TestUsersList.First(e=> e.Id == 2) },
            new ChatEvent() { Id = 22, UserId = 1, ChatEventType = ChatEventType.HighFiveAnotherPerson, OccuredTime = new DateTime(2020, 10, 22, 17, 27, 00), User = TestUsersList.First(e=> e.Id == 1)
                , HighFive = TestHighFivesList.First(e=> e.ChatEventId == 22)},
            new ChatEvent() { Id = 23, UserId = 1, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 22, 17, 28, 00), User = TestUsersList.First(e=> e.Id == 1) },
            new ChatEvent() { Id = 24, UserId = 3, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 22, 17, 29, 00), User = TestUsersList.First(e=> e.Id == 3) },
            new ChatEvent() { Id = 25, UserId = 2, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 22, 18, 30, 00), User = TestUsersList.First(e=> e.Id == 2) }
        };
    }
}
