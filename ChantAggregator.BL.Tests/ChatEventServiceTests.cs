using ChatAggregator.BL;
using ChatAggregator.BL.Data;
using ChatAggregator.BL.Data.Entities;
using ChatAggregator.BL.Enums;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ChantAggregator.BL.Tests
{
    public class ChatEventServiceTests
    {
        private readonly Mock<IChatEventRepository> _chatEventRepositoryMock;
        private readonly IChatEventService _chatEventService;

        public ChatEventServiceTests()
        {
            _chatEventRepositoryMock = new Mock<IChatEventRepository>();
            _chatEventService = new ChatEventService(_chatEventRepositoryMock.Object);
        }

        [Fact]
        public async Task Get_ChatEvent_Agregated_For_Granularity_None_Empty()
        {
            DateTime? from = new DateTime(2020, 10, 22);
            DateTime? till = new DateTime(2020, 10, 23);

            var chatEvents = await _chatEventService.GetChatEventsAgregatedAsync(from, till, ChatEventsGranularityType.None);

            //we should get no data
            Assert.Empty(chatEvents);
        }

        [Fact]
        public async Task Get_ChatEvent_Agregated_For_Granularity_Hourly_Success()
        {
            DateTime? from = new DateTime(2020, 10, 22);
            DateTime? till = new DateTime(2020, 10, 23);
            var _testChatEventsList = ChatEventServiceTestsData.TestChatEventsList.Where(e => e.OccuredTime >= from.Value && e.OccuredTime <= till.Value).ToList();
            _chatEventRepositoryMock.SetupSequence(e => e.GetChatEventsAsync(It.Is<DateTime?>(e => e == from), It.Is<DateTime?>(e => e == till))).ReturnsAsync(_testChatEventsList);


            var chatEvents = await _chatEventService.GetChatEventsAgregatedAsync(from, till, ChatEventsGranularityType.Hourly);

            //we should get this data
            Assert.Equal(2, chatEvents.Count);
            Assert.Equal(2, chatEvents.Count);
            Assert.Equal(2, chatEvents[0].CommentActionsCount);
            Assert.Equal(3, chatEvents[0].EnterTheRoomActionsCount);
            Assert.Equal(2, chatEvents[0].HighFiveAnotherPersonActionsCount);
            Assert.Equal(1, chatEvents[0].HighFiveAnotherPersonUsersCount);
            Assert.Equal(2, chatEvents[0].LeaveTheRoomActionsCount);
            Assert.Equal(1, chatEvents[1].LeaveTheRoomActionsCount);
        }

        [Fact]
        public async Task Get_ChatEvent_Agregated_For_Granularity_Daily_Success()
        {
            DateTime? from = new DateTime(2020, 10, 22);
            DateTime? till = new DateTime(2020, 10, 23);
            var _testChatEventsList = ChatEventServiceTestsData.TestChatEventsList.Where(e => e.OccuredTime >= from.Value && e.OccuredTime <= till.Value).ToList();
            _chatEventRepositoryMock.SetupSequence(e => e.GetChatEventsAsync(It.Is<DateTime?>(e => e == from), It.Is<DateTime?>(e => e == till))).ReturnsAsync(_testChatEventsList);


            var chatEvents = await _chatEventService.GetChatEventsAgregatedAsync(from, till, ChatEventsGranularityType.Daily);

            //we should get this data
            Assert.Single(chatEvents);
            Assert.Equal(2, chatEvents[0].CommentActionsCount);
            Assert.Equal(3, chatEvents[0].EnterTheRoomActionsCount);
            Assert.Equal(2, chatEvents[0].HighFiveAnotherPersonActionsCount);
            Assert.Equal(1, chatEvents[0].HighFiveAnotherPersonUsersCount);
            Assert.Equal(3, chatEvents[0].LeaveTheRoomActionsCount);
        }

        [Fact]
        public async Task Get_ChatEvent_Agregated_For_Granularity_Daily_Empty()
        {
            DateTime? from = new DateTime(2020, 04, 22);
            DateTime? till = new DateTime(2020, 05, 23);
            var _testChatEventsList = ChatEventServiceTestsData.TestChatEventsList.Where(e => e.OccuredTime >= from.Value && e.OccuredTime <= till.Value).ToList();
            _chatEventRepositoryMock.SetupSequence(e => e.GetChatEventsAsync(It.Is<DateTime?>(e => e == from), It.Is<DateTime?>(e => e == till))).ReturnsAsync(_testChatEventsList);


            var chatEvents = await _chatEventService.GetChatEventsAgregatedAsync(from, till, ChatEventsGranularityType.Daily);

            //we should get this data
            Assert.Empty(chatEvents);
        }
    }
}
