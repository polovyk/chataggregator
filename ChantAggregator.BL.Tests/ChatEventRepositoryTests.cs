using ChatAggregator.BL;
using ChatAggregator.BL.Data;
using ChatAggregator.BL.Data.Entities;
using ChatAggregator.BL.Enums;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ChantAggregator.BL.Tests
{
    public class ChatEventRepositoryTests
    {
        private readonly IChatEventRepository _chatEventRepository;

        public ChatEventRepositoryTests()
        {
            var dbContext = GetDbContext();
            _chatEventRepository = new ChatEventRepository(dbContext);
        }

        private ChatAggregatorContext GetDbContext()
        {
            var dbContextOptionsBuilder = new DbContextOptionsBuilder<ChatAggregatorContext>();
            var dbCOntextOptions = dbContextOptionsBuilder.UseSqlite("Data Source=blogging.db").Options;
            ChatAggregatorContext dbContext = new ChatAggregatorContext(dbCOntextOptions);

            return dbContext;
        }

        [Fact]
        public async Task Get_All_ChatEvent_From_Db_Success()
        {
            DateTime? from = null;
            DateTime? till = null;

            var chatEvents = await _chatEventRepository.GetChatEventsAsync(from, till);

            //we should get 25 events
            Assert.Equal(25, chatEvents.Count);
        }

        [Fact]
        public async Task Get_ChatEvent_For_One_Day_From_Db_Success()
        {
            DateTime? from = new DateTime(2020, 10, 22);
            DateTime? till = new DateTime(2020, 10, 23);

            var chatEvents = await _chatEventRepository.GetChatEventsAsync(from, till);

            //we should get 25 events
            Assert.Equal(10, chatEvents.Count);
        }
    }
}
