﻿using ChatAggregator.BL.Data;
using ChatAggregator.BL.Data.Entities;
using ChatAggregator.BL.Enums;
using ChatAggregator.BL.Model;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAggregator.BL
{
    public class ChatEventRepository : IChatEventRepository
    {
        private readonly ChatAggregatorContext _dbContext;

        public ChatEventRepository(ChatAggregatorContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<ChatEvent>> GetChatEventsAsync(DateTime? from, DateTime? till)
        {
            var events = await _dbContext.ChatEvents
                .Where(e => (!from.HasValue || e.OccuredTime >= from.Value)
                            && (!till.HasValue || e.OccuredTime <= till.Value))
                .Include(e => e.User)
                .Include(e => e.Comment)
                .Include(e => e.HighFive).ThenInclude(e => e.HighFivedUser)
                .ToListAsync();

            return events;
        }
    }
}
