﻿using ChatAggregator.BL.Data;
using ChatAggregator.BL.Data.Entities;
using ChatAggregator.BL.Enums;
using ChatAggregator.BL.Model;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAggregator.BL
{
    public class ChatEventService : IChatEventService
    {
        private readonly IChatEventRepository _chatEventRepository;

        public ChatEventService(IChatEventRepository chatEventRepository)
        {
            _chatEventRepository = chatEventRepository;
        }

        public async Task<List<ChatEventsAggregatedData>> GetChatEventsAgregatedAsync(DateTime? from, DateTime? till, ChatEventsGranularityType aggregationLevelType)
        {
            var result = new List<ChatEventsAggregatedData>();

            if (aggregationLevelType != ChatEventsGranularityType.None)
            {
                var events = await _chatEventRepository.GetChatEventsAsync(from, till);
                if (events.Any())
                {
                    switch (aggregationLevelType)
                    {
                        case ChatEventsGranularityType.Daily:
                            {
                                result = GetDailyGranularity(events);
                                break;
                            }
                        case ChatEventsGranularityType.Hourly:
                        default:
                            {
                                result = GetHourlyGranularity(events);
                                break;
                            }
                    }
                }
            }

            return result;
        }

        private List<ChatEventsAggregatedData> GetHourlyGranularity(List<ChatEvent> chatEvents)
        {
            var granulatedChatEventsList = chatEvents.GroupBy(row => new DateTime(row.OccuredTime.Year, row.OccuredTime.Month, row.OccuredTime.Day, row.OccuredTime.Hour, 0, 0).ToString("yyyy/MM/dd h tt"));

            var result = GetChatEventsAggregatedFromGranulatedData(granulatedChatEventsList);

            return result;
        }

        private List<ChatEventsAggregatedData> GetDailyGranularity(List<ChatEvent> chatEvents)
        {
            var granulatedChatEventsList = chatEvents.GroupBy(row => new DateTime(row.OccuredTime.Year, row.OccuredTime.Month, row.OccuredTime.Day).ToString("yyyy/MM/dd"));

            var result = GetChatEventsAggregatedFromGranulatedData(granulatedChatEventsList);

            return result;
        }

        private List<ChatEventsAggregatedData> GetChatEventsAggregatedFromGranulatedData(IEnumerable<IGrouping<string, ChatEvent>> granulatedChatEventsList)
        {
            var result = granulatedChatEventsList.Select(grp => new ChatEventsAggregatedData
            {
                GranularityItemValue = grp.Key,
                CommentActionsCount = grp.Count(e => e.ChatEventType == ChatEventType.Comment),
                EnterTheRoomActionsCount = grp.Count(e => e.ChatEventType == ChatEventType.EnterTheRoom),
                HighFiveAnotherPersonActionsCount = grp.Count(e => e.ChatEventType == ChatEventType.HighFiveAnotherPerson),
                HighFiveAnotherPersonUsersCount = grp.Where(e => e.ChatEventType == ChatEventType.HighFiveAnotherPerson).GroupBy(e => e.UserId).Count(),
                LeaveTheRoomActionsCount = grp.Count(e => e.ChatEventType == ChatEventType.LeaveTheRoom),
            }).ToList();

            return result;
        }
    }
}
