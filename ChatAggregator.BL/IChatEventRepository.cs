﻿using ChatAggregator.BL.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatAggregator.BL
{
    public interface IChatEventRepository
    {
        Task<List<ChatEvent>> GetChatEventsAsync(DateTime? from, DateTime? till);
    }
}