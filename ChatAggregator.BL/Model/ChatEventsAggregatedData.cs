﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatAggregator.BL.Model
{
    public class ChatEventsAggregatedData
    {
        public string GranularityItemValue { get; set; }

        public int EnterTheRoomActionsCount { get; set; }
        public int LeaveTheRoomActionsCount { get; set; }
        public int CommentActionsCount { get; set; }
        public int HighFiveAnotherPersonActionsCount { get; set; }
        public int HighFiveAnotherPersonUsersCount { get; set; }
    }
}
