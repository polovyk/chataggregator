﻿using ChatAggregator.BL.Enums;
using ChatAggregator.BL.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatAggregator.BL
{
    public interface IChatEventService
    {
        Task<List<ChatEventsAggregatedData>> GetChatEventsAgregatedAsync(DateTime? from, DateTime? till, ChatEventsGranularityType aggregationLevelType);
    }
}