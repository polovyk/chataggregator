﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ChatAggregator.BL.Migrations
{
    public partial class InitialData_added_new_data : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 16, 0, new DateTime(2020, 10, 22, 17, 21, 0, 0, DateTimeKind.Unspecified), 1 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 18, 2, new DateTime(2020, 10, 22, 17, 23, 0, 0, DateTimeKind.Unspecified), 1 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 20, 3, new DateTime(2020, 10, 22, 17, 25, 0, 0, DateTimeKind.Unspecified), 1 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 21, 0, new DateTime(2020, 10, 22, 17, 26, 0, 0, DateTimeKind.Unspecified), 2 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 22, 3, new DateTime(2020, 10, 22, 17, 27, 0, 0, DateTimeKind.Unspecified), 1 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 23, 1, new DateTime(2020, 10, 22, 17, 28, 0, 0, DateTimeKind.Unspecified), 1 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 25, 1, new DateTime(2020, 10, 22, 18, 30, 0, 0, DateTimeKind.Unspecified), 2 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Name" },
                values: new object[] { 3, "Mike" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Name" },
                values: new object[] { 4, "Den" });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 8, 0, new DateTime(2020, 10, 21, 17, 59, 20, 0, DateTimeKind.Unspecified), 3 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 9, 2, new DateTime(2020, 10, 21, 18, 21, 0, 0, DateTimeKind.Unspecified), 3 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 11, 3, new DateTime(2020, 10, 21, 18, 21, 30, 0, DateTimeKind.Unspecified), 3 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 13, 2, new DateTime(2020, 10, 21, 18, 23, 0, 0, DateTimeKind.Unspecified), 3 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 14, 1, new DateTime(2020, 10, 21, 18, 23, 10, 0, DateTimeKind.Unspecified), 3 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 17, 0, new DateTime(2020, 10, 22, 17, 22, 0, 0, DateTimeKind.Unspecified), 3 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 19, 2, new DateTime(2020, 10, 22, 17, 24, 0, 0, DateTimeKind.Unspecified), 3 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 24, 1, new DateTime(2020, 10, 22, 17, 29, 0, 0, DateTimeKind.Unspecified), 3 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 10, 0, new DateTime(2020, 10, 21, 18, 21, 10, 0, DateTimeKind.Unspecified), 4 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 12, 3, new DateTime(2020, 10, 21, 18, 22, 0, 0, DateTimeKind.Unspecified), 4 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 15, 1, new DateTime(2020, 10, 21, 18, 23, 57, 0, DateTimeKind.Unspecified), 4 });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "ChatEventId", "Text" },
                values: new object[] { 18, "Hello!" });

            migrationBuilder.InsertData(
                table: "HighFives",
                columns: new[] { "ChatEventId", "HighFivedUserId" },
                values: new object[] { 22, 2 });

            migrationBuilder.InsertData(
                table: "HighFives",
                columns: new[] { "ChatEventId", "HighFivedUserId" },
                values: new object[] { 20, 3 });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "ChatEventId", "Text" },
                values: new object[] { 9, "Hello!" });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "ChatEventId", "Text" },
                values: new object[] { 13, "Bye!" });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "ChatEventId", "Text" },
                values: new object[] { 19, "Hello!" });

            migrationBuilder.InsertData(
                table: "HighFives",
                columns: new[] { "ChatEventId", "HighFivedUserId" },
                values: new object[] { 11, 4 });

            migrationBuilder.InsertData(
                table: "HighFives",
                columns: new[] { "ChatEventId", "HighFivedUserId" },
                values: new object[] { 12, 3 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Comments",
                keyColumn: "ChatEventId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Comments",
                keyColumn: "ChatEventId",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Comments",
                keyColumn: "ChatEventId",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Comments",
                keyColumn: "ChatEventId",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "HighFives",
                keyColumn: "ChatEventId",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "HighFives",
                keyColumn: "ChatEventId",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "HighFives",
                keyColumn: "ChatEventId",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "HighFives",
                keyColumn: "ChatEventId",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);
        }
    }
}
