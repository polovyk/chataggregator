﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ChatAggregator.BL.Migrations
{
    public partial class InitialData_added_date : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 1,
                column: "OccuredTime",
                value: new DateTime(2020, 10, 21, 17, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 2,
                column: "OccuredTime",
                value: new DateTime(2020, 10, 21, 17, 5, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 3,
                column: "OccuredTime",
                value: new DateTime(2020, 10, 21, 17, 15, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 4,
                column: "OccuredTime",
                value: new DateTime(2020, 10, 21, 17, 17, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 5,
                column: "OccuredTime",
                value: new DateTime(2020, 10, 21, 17, 18, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 6,
                column: "OccuredTime",
                value: new DateTime(2020, 10, 21, 17, 20, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 7,
                column: "OccuredTime",
                value: new DateTime(2020, 10, 21, 17, 21, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 1,
                column: "OccuredTime",
                value: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 2,
                column: "OccuredTime",
                value: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 3,
                column: "OccuredTime",
                value: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 4,
                column: "OccuredTime",
                value: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 5,
                column: "OccuredTime",
                value: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 6,
                column: "OccuredTime",
                value: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 7,
                column: "OccuredTime",
                value: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
