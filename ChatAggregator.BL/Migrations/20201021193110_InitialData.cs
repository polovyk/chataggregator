﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ChatAggregator.BL.Migrations
{
    public partial class InitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "Bob" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Name" },
                values: new object[] { 2, "Kate" });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 1, 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 3, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 5, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 2, 0, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 4, 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 6, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 });

            migrationBuilder.InsertData(
                table: "ChatEvents",
                columns: new[] { "Id", "ChatEventType", "OccuredTime", "UserId" },
                values: new object[] { 7, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "ChatEventId", "Text" },
                values: new object[] { 3, "Hey, Kate - high five?" });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "ChatEventId", "Text" },
                values: new object[] { 6, "Oh, typical" });

            migrationBuilder.InsertData(
                table: "HighFives",
                columns: new[] { "ChatEventId", "HighFivedUserId" },
                values: new object[] { 4, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Comments",
                keyColumn: "ChatEventId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Comments",
                keyColumn: "ChatEventId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "HighFives",
                keyColumn: "ChatEventId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ChatEvents",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
