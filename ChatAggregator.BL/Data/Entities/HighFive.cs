﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ChatAggregator.BL.Data.Entities
{
    public class HighFive
    {
        [Key]
        [ForeignKey(nameof(ChatEvent))]
        public int ChatEventId { get; set; }

        [ForeignKey(nameof(HighFivedUser))]
        public int HighFivedUserId { get; set; }


        public ChatEvent ChatEvent { get; set; }
        public User HighFivedUser { get; set; }

    }
}
