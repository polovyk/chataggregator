﻿using ChatAggregator.BL.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ChatAggregator.BL.Data.Entities
{
    public class ChatEvent
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey(nameof(User))]
        public int UserId { get; set; }

        public ChatEventType ChatEventType { get; set; }

        public DateTime OccuredTime { get; set; }


        public User User { get; set; }
        public HighFive HighFive { get; set; }
        public Comment Comment { get; set; }
    }
}
