﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ChatAggregator.BL.Data.Entities
{
    public class Comment
    {
        [Key]
        [ForeignKey(nameof(ChatEvent))]
        public int ChatEventId { get; set; }

        [Required]
        public string Text { get; set; }


        public ChatEvent ChatEvent { get; set; }
    }
}
