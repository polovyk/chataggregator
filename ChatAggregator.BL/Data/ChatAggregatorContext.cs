﻿using ChatAggregator.BL.Data.Entities;
using ChatAggregator.BL.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChatAggregator.BL.Data
{
    public class ChatAggregatorContext : DbContext
    {
        public ChatAggregatorContext(DbContextOptions<ChatAggregatorContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<ChatEvent> ChatEvents { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<HighFive> HighFives { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User { Id = 1, Name = "Bob" },
                new User { Id = 2, Name = "Kate" },
                new User { Id = 3, Name = "Mike" },
                new User { Id = 4, Name = "Den" }
            );

            modelBuilder.Entity<ChatEvent>().HasData(
                new ChatEvent() { Id = 1, UserId = 1, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime (2020, 10, 21, 17, 00, 00) },
                new ChatEvent() { Id = 2, UserId = 2, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime(2020, 10, 21, 17, 5, 00) },
                new ChatEvent() { Id = 3, UserId = 1, ChatEventType = ChatEventType.Comment, OccuredTime = new DateTime(2020, 10, 21, 17, 15, 00) },
                new ChatEvent() { Id = 4, UserId = 2, ChatEventType = ChatEventType.HighFiveAnotherPerson, OccuredTime = new DateTime(2020, 10, 21, 17, 17, 00) },
                new ChatEvent() { Id = 5, UserId = 1, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 21, 17, 18, 00) },
                new ChatEvent() { Id = 6, UserId = 2, ChatEventType = ChatEventType.Comment, OccuredTime = new DateTime(2020, 10, 21, 17, 20, 00) },
                new ChatEvent() { Id = 7, UserId = 2, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 21, 17, 21, 00) },

                new ChatEvent() { Id = 8, UserId = 3, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime(2020, 10, 21, 17, 59, 20) },
                new ChatEvent() { Id = 9, UserId = 3, ChatEventType = ChatEventType.Comment, OccuredTime = new DateTime(2020, 10, 21, 18, 21, 00) },
                new ChatEvent() { Id = 10, UserId = 4, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime(2020, 10, 21, 18, 21, 10) },
                new ChatEvent() { Id = 11, UserId = 3, ChatEventType = ChatEventType.HighFiveAnotherPerson, OccuredTime = new DateTime(2020, 10, 21, 18, 21, 30) },
                new ChatEvent() { Id = 12, UserId = 4, ChatEventType = ChatEventType.HighFiveAnotherPerson, OccuredTime = new DateTime(2020, 10, 21, 18, 22, 00) },
                new ChatEvent() { Id = 13, UserId = 3, ChatEventType = ChatEventType.Comment, OccuredTime = new DateTime(2020, 10, 21, 18, 23, 00) },
                new ChatEvent() { Id = 14, UserId = 3, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 21, 18, 23, 10) },
                new ChatEvent() { Id = 15, UserId = 4, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 21, 18, 23, 57) },
                
                new ChatEvent() { Id = 16, UserId = 1, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime(2020, 10, 22, 17, 21, 00) },
                new ChatEvent() { Id = 17, UserId = 3, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime(2020, 10, 22, 17, 22, 00) },
                new ChatEvent() { Id = 18, UserId = 1, ChatEventType = ChatEventType.Comment, OccuredTime = new DateTime(2020, 10, 22, 17, 23, 00) },
                new ChatEvent() { Id = 19, UserId = 3, ChatEventType = ChatEventType.Comment, OccuredTime = new DateTime(2020, 10, 22, 17, 24, 00) },
                new ChatEvent() { Id = 20, UserId = 1, ChatEventType = ChatEventType.HighFiveAnotherPerson, OccuredTime = new DateTime(2020, 10, 22, 17, 25, 00) },
                new ChatEvent() { Id = 21, UserId = 2, ChatEventType = ChatEventType.EnterTheRoom, OccuredTime = new DateTime(2020, 10, 22, 17, 26, 00) },
                new ChatEvent() { Id = 22, UserId = 1, ChatEventType = ChatEventType.HighFiveAnotherPerson, OccuredTime = new DateTime(2020, 10, 22, 17, 27, 00) },
                new ChatEvent() { Id = 23, UserId = 1, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 22, 17, 28, 00) },
                new ChatEvent() { Id = 24, UserId = 3, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 22, 17, 29, 00) },
                new ChatEvent() { Id = 25, UserId = 2, ChatEventType = ChatEventType.LeaveTheRoom, OccuredTime = new DateTime(2020, 10, 22, 18, 30, 00) }
            );

            modelBuilder.Entity<Comment>().HasData(
                new Comment { ChatEventId = 3, Text = "Hey, Kate - high five?" },
                new Comment { ChatEventId = 6, Text = "Oh, typical" },
                new Comment { ChatEventId = 9, Text = "Hello!" },
                new Comment { ChatEventId = 13, Text = "Bye!" },
                new Comment { ChatEventId = 18, Text = "Hello!" },
                new Comment { ChatEventId = 19, Text = "Hello!" }
            );

            modelBuilder.Entity<HighFive>().HasData(
                new HighFive { ChatEventId = 4, HighFivedUserId = 1 },
                new HighFive { ChatEventId = 11, HighFivedUserId = 4 },
                new HighFive { ChatEventId = 12, HighFivedUserId = 3 },
                new HighFive { ChatEventId = 20, HighFivedUserId = 3 },
                new HighFive { ChatEventId = 22, HighFivedUserId = 2 }
            );
        }
    }
}
