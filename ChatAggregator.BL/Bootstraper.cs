﻿using ChatAggregator.BL.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChatAggregator.BL
{
    public static class Bootstraper
    {
        public static IServiceCollection AddBusinessLogicServices(this IServiceCollection services, BusinessLogicSettings settings)
        {
            services.AddDbContext<ChatAggregatorContext>(options
                    => options.UseSqlite(settings.ConnectionString)
                 );

            services.AddTransient<IChatEventService, ChatEventService>();
            services.AddTransient<IChatEventRepository, ChatEventRepository>();

            return services;
        }
    }
}
