﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ChatAggregator.BL.Enums
{
    public enum ChatEventType
    {
        [Description("enter-the-room")]
        EnterTheRoom = 0,
        [Description("leave-the-room")]
        LeaveTheRoom = 1,
        [Description("comment")]
        Comment = 2,
        [Description("high-five-another-user")]
        HighFiveAnotherPerson = 3
    }
}
