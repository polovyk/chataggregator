﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatAggregator.BL.Enums
{
    public enum ChatEventsGranularityType
    {
        None = 0,
        Hourly = 1,
        Daily = 2
    }
}
