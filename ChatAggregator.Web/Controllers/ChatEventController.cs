﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChatAggregator.BL;
using ChatAggregator.BL.Data.Entities;
using ChatAggregator.BL.Enums;
using ChatAggregator.BL.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ChatAggregator.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ChatEventController : ControllerBase
    {
        private readonly IChatEventService _chatEventService;
        private readonly IChatEventRepository _chatEventRepository;

        public ChatEventController(IChatEventService chatEventService, IChatEventRepository chatEventRepository)
        {
            _chatEventService = chatEventService;
            _chatEventRepository = chatEventRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<ChatEvent>> Get(DateTime? from, DateTime? till)
        {
            var chatEvents = await _chatEventRepository.GetChatEventsAsync(from, till);

            return chatEvents;
        }

        [HttpGet("getagregateddata")]
        public async Task<IEnumerable<ChatEventsAggregatedData>> GetAgregatedData(DateTime? from, DateTime? till, ChatEventsGranularityType aggregationLevelType)
        {
            var chatEventsAgregatedData = await _chatEventService.GetChatEventsAgregatedAsync(from, till, aggregationLevelType);

            return chatEventsAgregatedData;
        }
    }
}
