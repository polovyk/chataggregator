﻿import React, { Component } from 'react';
import DatePicker from 'react-datepicker';

import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';

export class ChatEvents extends Component {
    static displayName = ChatEvents.name;

    constructor(props) {
        super(props);

        this.state = {
            chatEvents: [],
            granularity: 0,
            from: new Date(new Date().setHours(0, 0, 0, 0)),
            till: new Date(),
            loading: true
        };

        this.handleChangeGranularity = this.handleChangeGranularity.bind(this);
        this.handleChangeFrom = this.handleChangeFrom.bind(this);
        this.handleChangeTill = this.handleChangeTill.bind(this);
    }

    componentDidMount() {
        const { from, till } = this.state;
        this.populateRawChatData(from, till);
    }

    handleChangeGranularity(event) {
        const granularity = event.target.value;
        this.setState({ granularity: granularity });

        const { from, till } = this.state;

        this.populateChatData(from, till, granularity);
    }

    handleChangeFrom(from) {
        this.setState({ from: from, loading: true });

        const { till, granularity } = this.state;

        this.populateChatData(from, till, granularity);

    }

    handleChangeTill(till) {
        this.setState({ till: till, loading: true });

        const { from, granularity } = this.state;

        this.populateChatData(from, till, granularity);
    }

    getChatEventDescription(event) {
        switch (event.chatEventType) {
            case 0: return `${event.user.name} enters-the-room`;
            case 1: return `${event.user.name} leaves-the-room`;
            case 2: return `${event.user.name} comments: "${event.comment.text}"`;
            case 3: return `${event.user.name} high-fives ${event.highFive.highFivedUser.name}`;
            default: return "";
        }
    }

    choosePersonOrPeoples(amount) {
        if (amount <= 0)
            return '';
        if (amount == 1)
            return 'person';
        if (amount > 1)
            return 'people';
    }

    formatEventOccuredTime(occuredTime) {
        let dateTime = new Date(occuredTime);

        let year = dateTime.getFullYear();

        let month = dateTime.getMonth() + 1;
        if (month < 10)
            month = "0" + month;

        let day = dateTime.getDate();
        if (day < 10)
            day = "0" + day;

        let hours = dateTime.getHours();

        let minutes = dateTime.getMinutes();
        if (minutes < 10)
            minutes = "0" + minutes;

        let postfix;

        if (hours < 12) {
            postfix = "am";
            if (hours == 0)
                hours = 12;
        }
        else {
            hours = hours - 12;
            postfix = "pm";
        }

        let result = `${year}/${month}/${day} ${hours}:${minutes}${postfix}`;

        return result;
    }

    renderFilters() {
        return (
            <div className="row">
                <div className="col-3 mt-1">
                    <label><b>Granularity:</b>&nbsp;</label>
                    <select value={this.state.granularity} onChange={this.handleChangeGranularity}>
                        <option value="0">None</option>
                        <option value="1">Hourly</option>
                        <option value="2">Daily</option>
                    </select>
                </div>
                <div className="col-4">
                    <label><b>From:</b>&nbsp;</label>
                    <DatePicker
                        selected={this.state.from}
                        onChange={this.handleChangeFrom}
                        selectsStart
                        showTimeSelect
                        timeIntervals={10}
                        dateFormat="yyyy/MM/dd hh:mm aa"
                    />
                </div>
                <div className="col-4">
                    <label><b>Till:</b>&nbsp;</label>
                    <DatePicker
                        selected={this.state.till}
                        onChange={this.handleChangeTill}
                        selectsEnd
                        showTimeSelect
                        timeIntervals={10}
                        minDate={this.state.from}
                        dateFormat="yyyy/MM/dd hh:mm aa"
                    />
                </div>
            </div>
        );
    }

    renderChatEvents(chatEvents) {

        return (
            <div>
                <div className="mb-4">{this.renderFilters()}</div>
                {
                    this.state.granularity == 0
                        ? (<div>{this.renderRawChatEvents(chatEvents)}</div>)
                        : (<div>{this.renderAgregatedChatEvents(chatEvents)}</div>)
                }
            </div>
        );
    }

    renderRawChatEvents(chatEvents) {
        return (
            < div className='row' >
                <div className='col-lg-9 col-sm-12'>
                    {chatEvents && chatEvents.length > 0
                        ? (
                            <table className='table table-striped' aria-labelledby="tabelLabel">
                                <tbody>
                                    {chatEvents.map(event =>
                                        <tr key={event.id}>
                                            <td>{this.formatEventOccuredTime(event.occuredTime)}:</td>
                                            <td>{this.getChatEventDescription(event)}</td>
                                        </tr>
                                    )}
                                </tbody>
                            </table>)
                        : (
                            <div>
                                No information for this time period
                            </div>)}
                </div>
            </div >
        );
    }

    renderAgregatedChatEvents(chatEvents) {
        return (
            <div>
                {chatEvents && chatEvents.length > 0
                    ? (<div>
                        {chatEvents.map(event =>
                            <div className="row mb-3" key={event.id}>
                                <div className="col-2">{event.granularityItemValue}:</div>
                                <div className="col">
                                    {event.enterTheRoomActionsCount && event.enterTheRoomActionsCount > 0
                                        ? (<div className="row">
                                            <div className="col">{`${event.enterTheRoomActionsCount} ${this.choosePersonOrPeoples(event.enterTheRoomActionsCount)} entered`}</div>
                                        </div>)
                                        : ('')
                                    }
                                    {event.leaveTheRoomActionsCount && event.leaveTheRoomActionsCount > 0
                                        ? (<div className="row">
                                            <div className="col">{`${event.leaveTheRoomActionsCount} left`}</div>
                                        </div>)
                                        : ('')
                                    }
                                    {event.highFiveAnotherPersonUsersCount && event.highFiveAnotherPersonUsersCount > 0
                                        ? (<div className="row">
                                            <div className="col">{`${event.highFiveAnotherPersonUsersCount} ${this.choosePersonOrPeoples(event.highFiveAnotherPersonUsersCount)} high-fived ${event.highFiveAnotherPersonActionsCount} other ${this.choosePersonOrPeoples(event.highFiveAnotherPersonActionsCount)}`}</div>
                                        </div>)
                                        : ('')
                                    }
                                    {event.commentActionsCount && event.commentActionsCount > 0
                                        ? (<div className="row">
                                            <div className="col">{`${event.commentActionsCount} comments`}</div>
                                        </div>)
                                        : ('')
                                    }
                                </div>
                            </div>)}
                    </div>)
                    : (<div>
                        No information for this time period
                    </div>)}
            </div>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderChatEvents(this.state.chatEvents);

        return (
            <div>
                <h1 id="tabelLabel" >Chat events</h1>
                {contents}
            </div>
        );
    }

    async populateChatData(from, till, granularity) {
        if (granularity == 0) {
            this.populateRawChatData(from, till);
        }
        else {
            this.populateAgregatedChatData(from, till, granularity);
        }
    }

    async populateRawChatData(from, till) {

        const url = `chatevent?from=${from.toISOString()}&till=${till.toISOString()}`;

        const response = await fetch(url);
        const data = await response.json();
        this.setState({ chatEvents: data, loading: false });
    }

    async populateAgregatedChatData(from, till, granularity) {

        const url = `chatevent/getagregateddata?from=${from.toISOString()}&till=${till.toISOString()}&aggregationLevelType=${granularity}`;

        const response = await fetch(url);
        const data = await response.json();
        this.setState({ chatEvents: data, loading: false });
    }
}
