# README #

### What is this repository for? ###

This project is using .Net Core 3.1, ReactJs and SQLite. May be required NodeJS to host React application.
It is made to help aggregate data from imaginary chat. There is no implemented chat functionality here.

### How do I get set up? ###

Project 'ChatAggregator.Web' contains the 'ClientApp' subdirectory with React application. If you open a command prompt in that directory, you can run npm commands. Before start make sure you have installed all npm dependencies. Use command `npm install` for this.

As a database is used SQLite database file 'blogging.db'. It is saved to repository in project 'ChatAggregator.Web'. This file already contais test data.
Database was created by EF Core CodeFist. DbContext can be found in project 'ChatAggregator.BL', subdirectory 'Data'. If you would use VS2019 Package Manager Console to update database - make sure that 'ChatAggregator.BL' is selected as a default project.

For tests a ceparate database file is used. Make sure that database file for tests will be copied to the 'ChantAggregator.BL.Tests' output directory.